import os
from pathlib import Path

_base_path = Path(__file__).resolve().parent


class Environ:

    def get(self, var_name):
        return os.environ.get(var_name)


class Database:

    def __init__(self):
        self.env = Environ()

    def uri(self):
        default_path = os.path.join(str(_base_path), 'app.db')

        env_uri = self.env.get('DATABASE_URL')
        default_uri = 'sqlite:///{}'.format(str(default_path))

        return env_uri or default_uri


_db = Database()
_env = Environ()


class Config:
    SQLALCHEMY_DATABASE_URI = _db.uri()
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    TOKEN_DURATION = 7
    SECRET_KEY = _env.get('SECRET_KEY') or 'fake_key'
