from datetime import datetime

from app import db

class BlacklistedToken(db.Model):
    __tablename__ = 'blacklisted_tokens'

    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String(500), index=True, unique=True, nullable=False)
    blacklisted_on = db.Column(db.DateTime, index=True, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return '<BlacklistedToken "{}">'.format(self.token)

    def is_blacklisted(self):
        return BlacklistedToken.query.filter_by(token=self.token).first() is not None
