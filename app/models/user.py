from datetime import datetime

from app import db
from app.models.chats_users import association_table


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Text, index=True, unique=True)
    email = db.Column(db.Text, index=True, unique=True)
    password_hash = db.Column(db.String(512))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    # Associations
    chats = db.relationship('Chat', secondary=association_table)
    messages = db.relationship('Message')

    def __repr__(self):
        return '<User "{}">'.format(self.username)

    def format_to_list(self):
        return {
            'id': self.id,
            'username': self.username
        }
