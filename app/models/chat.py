from datetime import datetime

from app import db
from app.models.chats_users import association_table


class Chat(db.Model):
    __tablename__ = 'chats'

    id = db.Column(db.Integer, primary_key=True)
    messages = db.relationship('Message')
    users = db.relationship('User', secondary=association_table)
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Chat "{}">'.format(self.id)

    def other_user(self, user):
        if len(self.users) > 2:
            return 'Grupo'

        other_user = next((u for u in self.users if u.id != user.id), user)
        return other_user

    def format_to_list(self, user):
        return {
            'id': self.id,
            'username': self.other_user(user).username,
            'preview': self.last_message()
        }

    def last_message(self):
        if self.messages:
            return self.messages[-1].message_body

    def room(self):
        return 'chat_{}'.format(self.id)
