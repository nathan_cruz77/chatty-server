from app import db

association_table = db.Table(
    'chats_users',
    db.Model.metadata,
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('chat_id', db.Integer, db.ForeignKey('chats.id'))
)
