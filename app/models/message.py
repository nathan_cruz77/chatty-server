from datetime import datetime

from app import db


class Message(db.Model):
    __tablename__ = 'messages'

    id = db.Column(db.Integer, primary_key=True)
    message_body = db.Column(db.Text)
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    # Associations
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User', foreign_keys=user_id)

    chat_id = db.Column(db.Integer, db.ForeignKey('chats.id'))
    chat = db.relationship('Chat', foreign_keys=chat_id)

    def __repr__(self):
        return '<Message "{}">'.format(self.id)

    def format_to_list(self):
        return {
            'id': self.id,
            'message_body': self.message_body,
            'created_at': self.created_at,
            'username': self.user.username
        }

    def to_json(self):
        formatted_message = self.format_to_list()

        del formatted_message['created_at']
        formatted_message['chatId'] = self.chat.id
        formatted_message['message'] = self.message_body

        return formatted_message
