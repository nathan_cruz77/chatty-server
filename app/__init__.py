from flask import Flask

# Extensions
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_socketio import SocketIO

from config import Config

app = Flask(__name__)
app.config.from_object(Config)

cors = CORS(app)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
socketio = SocketIO(app, async_mode='eventlet')

from app import models
from app import routes
