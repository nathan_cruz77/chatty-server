from flask import jsonify
from flask import request

from app import app
from app import db

from app.models import User

from app.auth.helpers import authenticated


@app.route('/user/search', methods=['POST'])
@authenticated
def search_user():
    query = request.get_json().get('query')

    if query is None:
        response = {'status': 'fail', 'message': 'Parâmetro de busca inválido'}
        return jsonify(response), 400

    users = User.query.filter(User.username.ilike('%{}%'.format(query)))

    return jsonify({
        'status': 'succes',
        'data': [u.format_to_list() for u in users]
    })
