from datetime import datetime
from datetime import timedelta

import jwt

from app import app
from app.models import BlacklistedToken


class TokenHandler:

    def __init__(self):
        self._token_duration = app.config.get('TOKEN_DURATION')
        self._secret = app.config.get('SECRET_KEY')

    def encode_token(self, user_id):
        payload = {
            'exp': self._expire_at(),
            'iat': datetime.utcnow(),
            'sub': user_id
        }

        return jwt.encode(payload, self._secret, algorithm='HS256')


    def decode_token(self, token):
        payload = jwt.decode(token, app.config.get('SECRET_KEY'))

        if BlacklistedToken(token=token).is_blacklisted():
            raise ValueError('Token is blacklisted')

        return payload['sub']

    def _expire_at(self):
        return datetime.utcnow() + timedelta(days=self._token_duration)
