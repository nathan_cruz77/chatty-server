from flask import request
from flask import jsonify

from app import app
from app import db

from app.models import User
from app.models import BlacklistedToken

from app.auth.token_handler import TokenHandler
from app.auth.helpers import authenticated

@app.route('/auth/register', methods=['POST'])
def register():
    data = request.get_json()
    user = User.query.filter_by(username=data.get('username')).first()

    handler = TokenHandler()

    if user:
        return jsonify({'status': 'fail', 'message': 'Usuário já existe'}), 409

    user = User(
        username=data.get('username'),
        password_hash=data.get('passwordHash')
    )

    db.session.add(user)
    db.session.commit()

    token = handler.encode_token(user.id)

    response = {
        'status': 'success',
        'message': 'Registrado com sucesso',
        'token': token.decode()
    }

    return jsonify(response), 201


@app.route('/auth/login', methods=['POST'])
def login():
    data = request.get_json()

    user = User.query.filter_by(
        username=data.get('username').strip(),
        password_hash=data.get('passwordHash')
    ).first()

    handler = TokenHandler()

    if not user:
        return jsonify({'status': 'fail', 'message': 'Usuário não existe'}), 404


    token = handler.encode_token(user.id)
    response = {
        'status': 'success',
        'message': 'Registrado com sucesso.',
        'token': token.decode()
    }

    return jsonify(response), 200


@app.route('/auth/logout')
def logout():
    token = request.headers.get('Authorization') or ''
    handler = TokenHandler()

    try:
        user_id = handler.decode_token(token)
    except Exception:
        return jsonify({'status': 'fail', 'message': 'Token inválido'}), 401

    blacklist_entry = BlacklistedToken(token=token)
    db.session.add(blacklist_entry)
    db.session.commit()

    return jsonify({'status': 'success', 'message': 'Deslogado'})


@app.route('/auth/status', methods=['GET'])
@authenticated
def status():
    response = {
        'status': 'success',
        'user': {
            'id': current_user.id,
            'username': current_user.username,
            'created_at': current_user.created_at
        }
    }

    return jsonify(response), 200
