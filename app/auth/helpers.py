from functools import wraps

from flask import request
from flask import jsonify
from flask import abort
from flask import make_response

from app import app
from app import socketio

from app.models import User
from app.auth.token_handler import TokenHandler


# The actual decorator function
def authenticated(function):
    @wraps(function)
    def decorated_function(*args, **kwargs):
        handler = TokenHandler()
        token = request.headers.get('Authorization').split(' ')[1]

        try:
            user_id = handler.decode_token(token)
            function.__globals__['current_user'] = User.query.get(user_id)
        except:
            response = make_response(
                jsonify({'status': 'fail', 'message': 'Token inválido'}),
                401
            )
            return abort(response)

        return function(*args, **kwargs)

    return decorated_function


# Token authentication for socket handlers
def socket_authenticated(function):
    @wraps(function)
    def decorated_function(*args):
        handler = TokenHandler()
        token = args[0].get('token')

        try:
            user_id = handler.decode_token(token)
            function.__globals__['current_user'] = User.query.get(user_id)
        except:
            with app.app_context():
                socketio.send('Invalid token')

            return

        return function(*args)

    return decorated_function
