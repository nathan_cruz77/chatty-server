from flask import jsonify
from flask import request

from flask_socketio import join_room

from app import app
from app import db
from app import socketio

from app.models import Chat
from app.models import User
from app.models import Message

from app.auth.helpers import authenticated
from app.auth.helpers import socket_authenticated


@socketio.on('heartbeat')
@socket_authenticated
def heartbeat(_):
    pass

@socketio.on('join_chats')
@socket_authenticated
def join_chats(data):
    print('Joining chats: {}'.format(current_user))

    for chat in current_user.chats:
        join_room(chat.room())


@app.route('/chat', methods=['GET'])
@authenticated
def load_chat_list():
    chats = [c.format_to_list(current_user) for c in current_user.chats]
    return jsonify({'status': 'success', 'chats': chats})


@app.route('/chat', methods=['POST'])
@authenticated
def create_chat():
    user_id = request.get_json().get('userId')
    user = User.query.get(user_id)

    if user is None:
        response = {'status': 'fail', 'message': 'Usuário inválido'}
        return jsonify(response), 400

    chat = Chat()
    chat.users.append(current_user)
    chat.users.append(user)

    db.session.add(chat)
    db.session.commit()

    # TODO: Force rejoin chats on client when starting new conversation
    # join_room(chat.room(), sid=current_user.username)

    response = {
        'id': chat.id,
        'username': user.username,
        'preview': ''
    }

    return jsonify(response), 201


@app.route('/chat/<int:chat_id>', methods=['GET'])
@authenticated
def load_chat(chat_id):
    chat = Chat.query.get(chat_id)

    if current_user not in chat.users:
        response = {'status': 'fail', 'message': 'Chat não pertence ao usuário'}
        return jsonify(response), 401

    response = chat.format_to_list(current_user)
    response['messages'] = [m.format_to_list() for m in chat.messages]

    return jsonify(response)


@app.route('/chat/<int:chat_id>', methods=['PUT'])
@authenticated
def add_message(chat_id):
    chat = Chat.query.get(chat_id)
    data = request.get_json()

    if current_user not in chat.users:
        response = {'status': 'fail', 'message': 'Chat não pertence ao usuário'}
        return jsonify(response), 401

    message = Message(
        chat=chat,
        user=current_user,
        message_body=data['messageBody'].strip()
    )

    db.session.add(message)
    db.session.commit()

    with app.app_context():
        socketio.emit('newMessage', message.to_json(), room=chat.room())

    return jsonify({'status': 'success', 'message': 'Mensagem cadastrada'}), 201
